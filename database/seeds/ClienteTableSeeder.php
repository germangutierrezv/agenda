<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class ClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create();
       	$faker= Faker::create();
        for($j=1; $j<=10; $j++)
        {
            $p= new App\Cliente();
            $p->nombre= $faker->name;
            $p->apellido= $faker->name;
            $p->correo= $faker->unique()->safeEmail;
            $p->celular= $faker->numerify('#############');
            $p->telefono1= $faker->numerify('#############');
            $p->telefono2= $faker->numerify('#############');

            $p->save();
        }
        
    }
}
