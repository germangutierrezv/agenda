<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use Validator;
use Illuminate\Support\Facades\Input;

class ClienteController extends Controller
{
    
    public function index()
    {

    	$clientes = Cliente::orderBy('id','asc')
    				->paginate(5);

        return view('cliente.index',['clientes'=>$clientes]);	
    }

    public function show($id)
    {
    	$cliente = Cliente::find($id);

    	if($cliente) {
    		return view('cliente.show',['cliente'=>$cliente]);
    	}
    }

    public function create()
    {
        return view('cliente.create');	
    }

    public function store(Request $request)
    {
    	$validation = $this->validator($request->all());

    	if($validation->fails()) {
    		return redirect()->back()->withErrors($validator->errors())->withInput();
    	}

    	$cliente = new Cliente();

    	$cliente->nombre = $request->nombre;
    	$cliente->apellido = $request->apellido;
    	$cliente->correo = $request->correo;
    	if($request->celular)
    		$cliente->celular = $request->celular;
    	if($request->telefono1)
    		$cliente->telefono1 = $request->telefono1;
    	if($request->telefono2)
    		$cliente->telefono2 = $request->telefono2;
    	
    	$cliente->save();

    	return redirect()->route('clientes.index')->with('mensaje','Cliente insertado correctamente.');
    }

    public function edit($id) 
    {
    	$cliente = Cliente::find($id);

   		return view('cliente.edit',['cliente'=>$cliente]);
    }

    public function update(Request $request, $id)
    {
		$validator = $this->validator($request->all());

    	if($validator->fails()) {

    	}

    	$cliente = Cliente::find($id);

    	$input = $request->all();

    	$cliente->fill($input)->save();

    	return redirect()->route('clientes.index')->with('mensaje','Cliente actualizado correctamente.');

    }

    public function destroy($id)
    {
    	$cliente = Cliente::find($id);

    	$cliente->delete();

    	return redirect('clientes')->with('mensaje','Cliente eliminado correctamente');
    }

    protected function validator(array $data)
    {
    	return Validator::make($data,[
    		'nombre' => 'required|string',
    		'apellido' => 'required|string',
    		'correo' => 'required|email'
    	]);
    }

    public function buscar()
    {
    	$buscar=Input::get('buscar');

    	if (empty($buscar)) {
    		return redirect()->back();    		
    	} 

    	$clientes = Cliente::where('nombre', 'LIKE', $buscar.'%')
    				->orWhere('celular', 'LIKE', $buscar.'%')
    				->orWhere('telefono1', 'LIKE', $buscar.'%')
    				->orWhere('telefono2', 'LIKE', $buscar.'%')
   					->orderBy('id','asc')
    				->paginate(5);

    	if($clientes->count()){
        	return view('cliente.resultado',['clientes'=>$clientes]);	
    	} else {
        	return view('cliente.resultado')->with('mensaje','No hay resultado de la busqueda');	
    	}
    	
    }
}
