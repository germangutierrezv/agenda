@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-lg-8">
            <div class="card">
                <div class="card-header"><strong>Actualizar Cliente # {{$cliente->id}}</strong></div>

                <div class="card-body">

                    <form class="form-horizontal" action="{{url('clientes',$cliente->id)}}" method="POST">
       					<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="_method" value="PUT">

                    	<div class="form-group">
				            <label for="nombre">Nombre</label>
				            <span class="text-danger">*</span>
				            <input id ="nombre" type="text" autofocus class="form-control" name="nombre" value="{{ $cliente->nombre }}" required >
						</div>
                    	<div class="form-group">
				            <label for="apellido">Apellido</label>
				            <span class="text-danger">*</span>
				            <input id ="apellido" type="text" class="form-control" name="apellido" value="{{ $cliente->apellido }}" required >
						</div>
                    	<div class="form-group">
				            <label for="correo">Correo Electrónico</label>
				            <input id ="correo" type="email" class="form-control" name="correo" value="{{ $cliente->correo }}" >
						</div>
						<div class="form-group">
				            <label for="celular">Celular</label>
				            <input id ="celular" type="text" class="form-control" name="celular" value="{{ $cliente->celular }}" >
						</div>
						<div class="form-group">
				            <label for="telefono1">Telefono 01</label>
				            <input id ="apellido" type="text" class="form-control" name="telefono1" value="{{ $cliente->telefono1 }}" >
						</div>
						<div class="form-group">
				            <label for="telefono2">Telefono 02</label>
				            <input id ="apellido" type="text" class="form-control" name="telefono2" value="{{ $cliente->apellido }}" >
						</div>
						<button class="btn btn-success">Actualizar</button>
						<a class="btn btn-primary" href="{{url('clientes')}}">Volver</a>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection