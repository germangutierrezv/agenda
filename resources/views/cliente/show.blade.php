@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header"><strong>Detalle del Cliente # {{ $cliente->id }}</strong></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="table-responsive">
		                    <table class="table table-bordered table-hover table-striped">
		                        <thead>
		                        </thead>
		                        <tbody>
		                                <tr>
			                                <td><strong>ID</strong></td>
			                                <td>{{ $cliente->id }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Nombre</strong></td>
			                                <td>{{ $cliente->nombre }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Apellido</strong></td>
			                                <td>{{ $cliente->apellido }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Correo Electrónico</strong></td>
			                                <td>{{ $cliente->correo }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Celular</strong></td>
			                                <td>{{ $cliente->celular }}</td>
			                            <tr>
			                      		<tr>
			                                <td><strong>Telefono 01</strong></td>
			                                <td>{{ $cliente->telefono1 }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Telefono 02</strong></td>
			                                <td>{{ $cliente->telefono2 }}</td>
			                            <tr>		                       		                       		                        
			                    </tbody>
		                    </table>
		                </div>
		                <div>
		                	<a href="{{url('/clientes')}}" class="btn btn-primary">Volver</a>
		                </div> 
                </div>
            </div>
        </div>
    </div>
</div>

@endsection