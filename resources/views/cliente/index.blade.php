@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header"><strong>Listado de Clientes</strong></div>

                <div class="card-body">
                    @if (isset($mensaje))
                        <div class='bg-warning' style='padding: 10px'>
                            {{$mensaje}}
                        </div>
                    @endif
                    <div class="col-md-12">
                    	<a href="{{url('clientes/create')}}" class="btn btn-primary"><span class="fa fa-plus"></span> Crear Cliente</a>
                    </div>
                    <br>
                    @if (isset($clientes))
                    <div class="col-md-offset-10">
				    	<div class="form-group">
				            <div class="input-group col-md-6 pull-right">
				                <span class="input-group-addon">
				                	<form action="{{ url('clientes/buscar') }}" method="POST">
                                        <div class="form-group">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input id="buscar" type="text" class="form-control" name="buscar">
                                        </div>
                                        <button type="submit" class="btn btn-primary pull-right">Buscar</button>
                                    </form>
				                </span>   
				            </div>
						</div>
			    	</div>
                    <div class="table-responsive">
                    	<table class="table">
                    		<thead>
                    			<tr>
                    				<th>#</th>
                    				<th>Nombre</th>
                    				<th>Apellido</th>
                    				<th>Correo Eléctronico</th>
                    				<th>Celular</th>
                    				<th></th>
                    			</tr>
                    		</thead>
                    		<tbody>
                    			@foreach($clientes as $cliente)
                    			<tr>
                                    <td>
	                                	<a href="{{ url('clientes',$cliente->id) }}">
	                                		{{ $cliente->id }}
	                                	</a>
	                                </td>
	                                <td><a href="{{ url('clientes',$cliente->id) }}">
	                                		{{ $cliente->nombre }}
	                                	</a>
	                                </td>
	                                <td><a href="{{ url('clientes',$cliente->id) }}">
	                                		{{ $cliente->apellido }}
	                                	</a>
	                                </td>
	                                <td><a href="{{ url('clientes',$cliente->id) }}">
	                                		{{ $cliente->correo }}
	                                	</a>
	                                </td>
	                                <td><a href="{{ url('clientes',$cliente->id) }}">
	                                		{{ $cliente->celular }}
	                                	</a>
	                                </td>
	                                <td>
	                                	<a href="{{url('/clientes',$cliente->id)}}" class="btn btn-success">Ver</a>
	                                	<a href="{{route('clientes.edit',$cliente->id)}}" class="btn btn-warning">Editar</a>
                                        <form action="{{ url('clientes', $cliente->id) }}" method="POST" onsubmit="if(confirm('Estas seguro de Eliminar el cliente?')){ return true} else {return false};">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button type="submit" class="btn btn-danger pull-right" title="Eliminar Cliente">Eliminar</button>
                                                
                                        </form>
	                                </td>
                    			</tr>
                    			@endforeach
                    		</tbody>
                    		
                    	</table>
						<div class="">
							{{ $clientes->links() }}
						</div>
                    </div>
                    @else
                        <h2>No hay resultado</h2>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection