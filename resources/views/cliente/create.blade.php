@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-lg-8">
            <div class="card">
                <div class="card-header"><strong>Crear Cliente</strong></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" action="{{url('clientes')}}" method="POST">
       					<input type="hidden" name="_token" value="{{ csrf_token() }}">

                    	<div class="form-group">
				            <label for="nombre">Nombre</label>
				            <span class="text-danger">*</span>
				            <input id ="nombre" type="text" autofocus class="form-control" name="nombre" required >
						</div>
                    	<div class="form-group">
				            <label for="apellido">Apellido</label>
				            <span class="text-danger">*</span>
				            <input id ="apellido" type="text" class="form-control" name="apellido" required >
						</div>
                    	<div class="form-group">
				            <label for="correo">Correo Electrónico</label>
				            <input id ="correo" type="email" class="form-control" name="correo" >
						</div>
						<div class="form-group">
				            <label for="celular">Celular</label>
				            <input id ="celular" type="text" class="form-control" name="celular" >
						</div>
						<div class="form-group">
				            <label for="telefono1">Telefono 01</label>
				            <input id ="apellido" type="text" class="form-control" name="telefono1" >
						</div>
						<div class="form-group">
				            <label for="telefono2">Telefono 02</label>
				            <input id ="apellido" type="text" class="form-control" name="telefono2" >
						</div>
						<button class="btn btn-success">Guardar</button>
						<a class="btn btn-primary" href="{{url('clientes')}}">Volver</a>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection